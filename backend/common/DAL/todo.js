const AWS = require('aws-sdk');
const { v4: uuidv4 } = require('uuid');

const todoFactory = (docClient, tableName) =>
    class Todo {
        constructor(todo) {
            this.id = todo.id || uuidv4();
            this.title = todo.title;
        }

        async create() {
            const params = {
                TableName: tableName,
                Item: {
                    id: this.id,
                    title: this.title
                }
            }
            await docClient.put(params).promise();
            return this;
        }

        async update() {
            const params = {
                TableName: tableName,
                Key: { id: this.id },
                ConditionExpression: '#id = :id',
                UpdateExpression: `set #title = :title`,
                ExpressionAttributeValues: {
                    ":id": this.id,
                    ":title": this.title
                },
                ExpressionAttributeNames: {
                    "#id": "id",
                    "#title": "title"
                },
            };

            await docClient.update(params).promise();
            return this;
        }

        async delete() {
            const params = {
                TableName: tableName,
                Key: {
                    id: this.id,
                },
                ConditionExpression: '#id = :id',
                ExpressionAttributeValues: {
                    ':id': this.id,
                },
                ExpressionAttributeNames: {
                    '#id': 'id',
                },
            };
            return docClient.delete(params).promise();
        }

        async list() {
            const params = {
                TableName: tableName,
            };
            return docClient.scan(params).promise();
        }
    }

const docClient = new AWS.DynamoDB.DocumentClient();
module.exports = {
    Todo: todoFactory(docClient, process.env.TODO_TABLE_NAME)
}