const { TodoManager } = require('common');

const deleteHandler = async (event, context) => {
    const { id } = event.pathParameters;
    try {
        const result = await TodoManager.delete(id);
        return {
            statusCode: 204,
            headers: {
                "Access-Control-Allow-Headers" : "*",
                "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify(
                result,
                null,
                2
            ),
        };
    } catch (error) {
        return {
            statusCode: 500,
            body: JSON.stringify(
                error,
                null,
                2
            ),
        };
    }
}

module.exports = { deleteHandler };
