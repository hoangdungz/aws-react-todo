const schema = require('../schema/update.json');
const { TodoManager } = require('common');
const { Validator } = require('jsonschema');

const update = async (event, context) => {
    const payload = JSON.parse(event.body);
    const validator = new Validator();
    const checkValidator = validator.validate(payload, schema);

    if(checkValidator.errors.length > 0) {
        return {
            statusCode: 400,
            body: JSON.stringify(
                { "message": "payload is invalid" },
                null,
                2
            ),
        }
    }
    try {
        const result = await TodoManager.update(payload);
        return {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Headers" : "*",
                "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify(
                result,
                null,
                2
            ),
        };
    } catch (error) {
        return {
            statusCode: 500,
            body: JSON.stringify(
                error,
                null,
                2
            ),
        };
    }
}

module.exports = { update };
