const { TodoManager } = require('common');

const list = async (event, context) => {
    try {
        const result = await TodoManager.list();
        return {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Headers" : "*",
                "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify(
                result,
                null,
                2
            ),
        };
    } catch (error) {
        return {
            statusCode: 500,
            body: JSON.stringify(
                error,
                null,
                2
            ),
        };
    }
}

module.exports = { list };
